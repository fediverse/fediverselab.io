
---
layout: "post"
title: "Friendica 2018.09"
date: 2018-09-24
tags:
    - friendica
preview: "This release brings the restructuring of the item table in the database, alongside some 100 closed issues from the tracker"
url: "https://friendi.ca/2018/09/23/friendica-2018-09-released"
lang: en
---

This release brings the restructuring of the item table in the database, alongside some 100 closed issues from the tracker. Other changes include translation updates, new configuration structure, possibility for admins to block certain nicknames and to mark a node for explicit content, and addition of private flag to API results.

You'll find more information in the official [announcement](https://friendi.ca/2018/09/23/friendica-2018-09-released).
