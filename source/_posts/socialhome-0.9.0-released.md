
---
layout: "post"
title: "Socialhome 0.9.0 release"
date: 2018-07-21
tags:
    - socialhome
preview: "This release brings support for limited content, notifications on mentions, tag search, new local content stream and user data export"
url: "https://socialhome.network/content/1824491/socialhome-090-released-httpssocial"
lang: en
---

This release brings a few long awaited features and privacy related improvements.
Users can now delete and export their data, and admins can publish terms and privacy documents.
Other main features include support for limited content, notifications on mentions, admin improvements, tag search and the new local content stream.
Read full [changelog](https://socialhome.network/content/1824491/socialhome-090-released-httpssocial) for more details.
